import os
import json
import boto3
from http import HTTPStatus

dynamodb = boto3.client('dynamodb',
                        region_name=os.environ['AWS_DEFAULT_REGION'],
                        aws_access_key_id=os.environ['AWS_ACCESS_KEY_ID'],
                        aws_secret_access_key=os.environ['AWS_SECRET_ACCESS_KEY'])


def lambda_handler(event, context):

    post = json.loads(event['body'])

    response = dynamodb.put_item(
        TableName='string',
        Item=post
    )

    return {
        'isBase64Encoded': False,
        'statusCode': HTTPStatus.OK,
        'headers': {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': True,
        },
        'multiValueHeaders': dict(),
        'body': json.dumps(response)
    }
