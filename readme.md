# Backend Test

This project uses the [Serverless Framework](https://serverless.com), Python 3.8, AWS Lambda, AWS API Gateway, AWS DynamoDB, AWS S3.

## The test

1. Your task is to build and deploy on AWS an API with a CRUD of **Blog posts** using the Serverless Framework, Lambda functions and DynamoDB.
   
2. The Blog posts can have medias attached to them, in that case, Create and Delete medias must also have endpoints. You should store the medias uploaded on a S3 Bucket and save its url on the post 'medias' array field.

3. Obs:
   - At task completion, make a pull request to this repository with the API url at the end of the README.md
   - The [Create function](/src/api/create_post.py) and [Insert Media](src/api/insert_media.py) are already implemented as an example, but you'll have to modify it to fit the requirements
   - If you have any trouble to complete this task you can add comments to the Problems section of this README explaining your situation
   - Each blog post must have the following schema, and you have to **make sure** the API doesn't allow different objects and that it returns the proper HTTP codes

```python
# Blog Post Object description

# _id: identifier uuid string
# author: string 
# title: string
# body: string
# medias: List[string]

{
    '_id': uuid,
    'author': str,
    'title': str,
    'body': str,
    'medias': list
}
```

## Tools

We encourage you to read the documentation of the tools used in this task:

- [Boto3](https://boto3.amazonaws.com/v1/documentation/api/latest/index.html)
- [Serverless](https://www.serverless.com/framework/docs/)

## Environment

### AWS
You are required to create an [AWS account](https://aws.amazon.com) if you don't have one yet.

### Setting up AWS
1. Create AWS credentials including the following IAM policies: ```AWSLambdaFullAccess```, ```AmazonS3FullAccess```, ```AmazonAPIGatewayAdministrator``` and ```AWSCloudFormationFullAccess```.
2. Set the ```AWS_ACCESS_KEY_ID``` and ```AWS_SECRET_ACCESS_KEY``` variables in your environment.

## Problems

## API url goes here