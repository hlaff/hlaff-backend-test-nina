from typing import List
from .db import dynamodb
from botocore.exceptions import ClientError
from fastapi.responses import JSONResponse
from boto3.dynamodb.conditions import Key

table = dynamodb.Table("blog_posts")

# Cria Post Blog
def create_blog_posts(blog_posts: dict):
    try:
        table.put_item(Item=blog_posts)
        return blog_posts
    except ClientError as e:
        return JSONResponse(content=e.response["Error"], status_code=500)

# Pega e retorna o Post Blog pelo ID
def get_blog_posts(id: str):
    try:
        response = table.query(
            KeyConditionExpression=Key("id").eq(id)
        )
        return response["Items"]
    except ClientError as e:
        return JSONResponse(content=e.response["Error"], status_code=500)

# Retorna todos os Posts Blog
def get_all_blog_posts():
    try:
        response = table.scan(
            Limit=5,
            AttributesToGet=["id", "author", "created_at", "title", "body", "medias"]
        )
        return response["Items"]
    except ClientError as e:
        return JSONResponse(content=e.response["Error"], status_code=500)

# Deleta o Post Blog pelo ID
def delete_blog_posts(blog_posts: dict):
    try:
        response = table.delete_item(
            Key={
                "id": blog_posts["id"],
                "created_at": blog_posts["created_at"]
            }
        )
        return response
    except ClientError as e:
        return JSONResponse(content=e.response["Error"], status_code=500)

# Atualiza o post blog
def update_blog_posts(blog_posts: dict):
    try:
        response = table.update_item(
            Key={
                "id": blog_posts["id"],
                "created_at": blog_posts["created_at"]
            },
            UpdateExpression="SET body = :body, title = :title, author = :author, medias = :medias",
            ExpressionAttributeValues={
                ":body": blog_posts["body"],
                ":title": blog_posts["title"],
                ":author": blog_posts["author"],
                ":medias": blog_posts["medias"]
            }
        )
        return response
    except ClientError as e:
        return JSONResponse(content=e.response["Error"], status_code=500)

# Inseri medias
def insert_medias(id: str, created_at: str, media: str):
    result = table.update_item(
        Key={
            'id': id,
            'created_at': created_at
        },
        UpdateExpression="SET medias = list_append(medias, :i)",
        ExpressionAttributeValues={
            ':i': [media],
        },
        ReturnValues="UPDATED_NEW"
    )
    if result['ResponseMetadata']['HTTPStatusCode'] == 200 and 'Attributes' in result:
        return result['Attributes']['medias']
    


