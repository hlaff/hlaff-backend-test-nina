from fastapi import FastAPI
from database.db import create_tables
from routes.blog_posts import routes_blog_posts


app = FastAPI()


app.include_router(routes_blog_posts, prefix="/blog_posts")

create_tables()