from typing import List
from fastapi import APIRouter
from models.blog_posts import BlogPosts
from database.blog_posts import create_blog_posts, get_all_blog_posts, insert_medias
from database.blog_posts import get_blog_posts
from database.blog_posts import get_blog_posts
from database.blog_posts import delete_blog_posts
from database.blog_posts import update_blog_posts


routes_blog_posts = APIRouter()

# CREATE blog_posts


@routes_blog_posts.post("/create", response_model=BlogPosts)
def create(blog_posts: BlogPosts):
    return create_blog_posts(blog_posts.dict())

# GET blog_posts BY ID


@routes_blog_posts.get("/get/{id}")
def get_by_id(id: str):
    return get_blog_posts(id)

# GET ALL BLOG POSTS


@routes_blog_posts.get("/all")
def get_all():
    return get_all_blog_posts()

# DELETE blog_posts


@routes_blog_posts.post("/delete")
def create(blog_posts: BlogPosts):
    return delete_blog_posts(blog_posts.dict())

# UPDATE blog_posts


@routes_blog_posts.post("/update")
def create(blog_posts: BlogPosts):
    return update_blog_posts(blog_posts.dict())


@routes_blog_posts.post("/detail/{id}/{created_at}/{media}")
def insert_media(id: str, created_at: str, media: str):
    return insert_medias(id, created_at, media)