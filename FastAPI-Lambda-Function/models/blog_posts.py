from datetime import datetime
from pydantic import BaseModel, Field
from uuid import uuid4

def generate_id():
    return str(uuid4())

def generate_date():
    return str(datetime.now())

class BlogPosts(BaseModel):
    id: str = Field(default_factory=generate_id)
    title: str
    author: str
    body: str
    medias: list
    created_at: str = Field(default_factory=generate_date)